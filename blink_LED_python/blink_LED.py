import RPi.GPIO as GPIO
import time
# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
while (1):
	GPIO.output(12, True)
	GPIO.output(7, True)
	time.sleep(.5)
	GPIO.output(12,False)
	GPIO.output(7, False)
	time.sleep(.5)